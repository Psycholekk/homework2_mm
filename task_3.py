#!/usr/bin/env python3


def greetings() -> None:
    user_name: str = input("Input your name: ")
    if user_name == "Milosz":
        print("Hello dear {}".format(user_name))
    else:
        print("Hello {}".format(user_name))


greetings()
print("-" * 20)


class Triangle:
    def __init__(self, height: float, base: float):
        self.height = height
        self.base = base

    def __eq__(self, other) -> bool:
        return self.height == other.height and self.base == other.base

    def __lt__(self, other) -> bool:
        return self.height < other.height and self.base < other.base

    def __gt__(self, other) -> bool:
        return self.height > other.height and self.base > other.base

    def __le__(self, other) -> bool:
        return self.height <= other.height and self.base <= other.base

    def __ge__(self, other) -> bool:
        return self.height >= other.height and self.base >= other.base

    def __ne__(self, other) -> bool:
        return self.height != other.height and self.base != other.base


class Sphere:

    def __init__(self, volume: float):
        self.volume = volume

    def __eq__(self, other) -> bool:
        return self.volume == other.volume

    def __lt__(self, other) -> bool:
        return self.volume < other.volume

    def __gt__(self, other) -> bool:
        return self.volume > other.volume

    def __le__(self, other) -> bool:
        return self.volume <= other.volume

    def __ge__(self, other) -> bool:
        return self.volume >= other.volume

    def __ne__(self, other) -> bool:
        return self.volume != other.volume


class Vehicle:

    def __init__(self, weight: float):
        self.weight = weight

    def __eq__(self, other) -> bool:
        return self.weight == other.weight

    def __lt__(self, other) -> bool:
        return self.weight < other.weight

    def __gt__(self, other) -> bool:
        return self.weight > other.weight

    def __le__(self, other) -> bool:
        return self.weight <= other.weight

    def __ge__(self, other) -> bool:
        return self.weight >= other.weight

    def __ne__(self, other) -> bool:
        return self.weight != other.weight


class Tank(Vehicle):

    def __init__(self, weight: float, gun_caliber: int):
        super().__init__(weight)
        self.gun_caliber = gun_caliber

    def __eq__(self, other) -> bool:
        return super().__eq__(other) and self.gun_caliber == other.gun_caliber

    def __lt__(self, other) -> bool:
        return super().__lt__(other) and self.gun_caliber < other.gun_caliber

    def __gt__(self, other) -> bool:
        return super().__gt__(other) and self.gun_caliber > other.gun_caliber

    def __le__(self, other) -> bool:
        return super().__le__(other) and self.gun_caliber <= other.gun_caliber

    def __ge__(self, other) -> bool:
        return super().__ge__(other) and self.gun_caliber >= other.gun_caliber

    def __ne__(self, other) -> bool:
        return super().__ne__(other) and self.gun_caliber != other.gun_caliber


class Heavy(Tank):

    def __init__(self, weight: float, gun_caliber: int, armor_thickness: int):
        super().__init__(weight, gun_caliber)
        self.armor_thickness = armor_thickness

    def __eq__(self, other) -> bool:
        return super().__eq__(other) and self.armor_thickness == other.armor_thickness

    def __lt__(self, other) -> bool:
        return super().__lt__(other) and self.armor_thickness < other.armor_thickness

    def __gt__(self, other) -> bool:
        return super().__gt__(other) and self.armor_thickness > other.armor_thickness

    def __le__(self, other) -> bool:
        return super().__le__(other) and self.armor_thickness <= other.armor_thickness

    def __ge__(self, other) -> bool:
        return super().__ge__(other) and self.armor_thickness >= other.armor_thickness

    def __ne__(self, other) -> bool:
        return super().__ne__(other) and self.armor_thickness != other.armor_thickness


class Medium(Tank):

    def __init__(self, weight: float, gun_caliber: int, gun_depression: int):
        super().__init__(weight, gun_caliber)
        self.gun_depression = gun_depression

    def __eq__(self, other) -> bool:
        return super().__eq__(other) and self.gun_depression == other.gun_depression

    def __lt__(self, other) -> bool:
        return super().__lt__(other) and self.gun_depression < other.gun_depression

    def __gt__(self, other) -> bool:
        return super().__gt__(other) and self.gun_depression > other.gun_depression

    def __le__(self, other) -> bool:
        return super().__le__(other) and self.gun_depression <= other.gun_depression

    def __ge__(self, other) -> bool:
        return super().__ge__(other) and self.gun_depression >= other.gun_depression

    def __ne__(self, other) -> bool:
        return super().__ne__(other) and self.gun_depression != other.gun_depression


class Light(Tank):

    def __init__(self, weight: float, gun_caliber: int, engine_power: int):
        super().__init__(weight, gun_caliber)
        self.engine_power = engine_power

    def __eq__(self, other) -> bool:
        return super().__eq__(other) and self.engine_power == other.engine_power

    def __lt__(self, other) -> bool:
        return super().__lt__(other) and self.engine_power < other.engine_power

    def __gt__(self, other) -> bool:
        return super().__gt__(other) and self.engine_power > other.engine_power

    def __le__(self, other) -> bool:
        return super().__le__(other) and self.engine_power <= other.engine_power

    def __ge__(self, other) -> bool:
        return super().__ge__(other) and self.engine_power >= other.engine_power

    def __ne__(self, other) -> bool:
        return super().__ne__(other) and self.engine_power != other.engine_power


class TankDestroyer(Tank):

    def __init__(self, weight: float, gun_caliber: int, penetration: int):
        super().__init__(weight, gun_caliber)
        self.penetration = penetration

    def __eq__(self, other) -> bool:
        return super().__eq__(other) and self.penetration == other.penetration

    def __lt__(self, other) -> bool:
        return super().__lt__(other) and self.penetration < other.penetration

    def __gt__(self, other) -> bool:
        return super().__gt__(other) and self.penetration > other.penetration

    def __le__(self, other) -> bool:
        return super().__le__(other) and self.penetration <= other.penetration

    def __ge__(self, other) -> bool:
        return super().__ge__(other) and self.penetration >= other.penetration

    def __ne__(self, other) -> bool:
        return super().__ne__(other) and self.penetration != other.penetration


su130pm1 = TankDestroyer(55, 155, 204)
su130pm2 = TankDestroyer(55, 155, 204)
hetzer = TankDestroyer(38, 87, 145)
print("Is SU-130PM '1' similar to SU-130PM '2'?")
print(su130pm1 == su130pm2)
print("Is SU-130PM '1' similar to Hetzer 38t?")
print(su130pm1 == hetzer)
