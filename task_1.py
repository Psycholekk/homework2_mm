#!/usr/bin/env python3


class Animal:
    def __init__(self):
        self.description = "Animal being"


class Dog(Animal):
    pass


class Weapon:
    def shot(self) -> None:
        print("Pif paf!")


class Glock(Weapon):
    pass


class Bazooka(Weapon):
    def shot(self) -> None:
        super().shot()
        print("Boom!")


Czarek = Dog()
print(Czarek.description)
print("-"*20)

pistol = Glock()
pistol.shot()
print("-"*20)

bazooka = Bazooka()
bazooka.shot()
