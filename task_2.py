#!/usr/bin/env python3


class Bluebell:
    def __init__(self, color: str, height: int):
        self.color = color
        self.height = height


def get_number() -> None:
    number: str = input("Input int number: ")
    number: int = int(number)
    print("Your number is: {}".format(number))


user_name: str = input("Type your name: ")
user_surname: str = input("Type your surname: ")
user_age: str = input("Type your age: ")
print(user_name, user_surname, user_age)
print("-"*20)

get_number()
print("-"*20)

color_in: str = input("Input your favourite flower color: ")
height_in: str = input("How tall is your flower in cm: ")
my_flower = Bluebell(color_in, int(height_in))
print(my_flower.color)
print(my_flower.height)
